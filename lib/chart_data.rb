module ChartData
  class Api
    FIELDS = {
        CREATED_AT: 2,
        STATUS: 3,
        DURATION: 4,
        WORKER_TIME: 5,
    }

    def initialize(data, start_time, end_time)
      @data = data
      @start = start_time.to_time
      @end = end_time.to_time
    end

    def build_duration
      result = []
      @data.each do |row|
        next unless filter row
        result << {
            duration: row[FIELDS[:DURATION]],
            created_at: row[FIELDS[:CREATED_AT]].to_time.strftime("%Y-%m-%d %H:%m")
        }
      end
      result
    end

    def builds_per_day
      result = {}
      @data.each do |row|
        next unless filter row
        status = row[FIELDS[:STATUS]].to_sym
        date = row[FIELDS[:CREATED_AT]].to_time.strftime("%Y-%m-%d")
        result[date] = {
            created_at: date,
            failed: 0,
            error: 0,
            passed: 0,
            stopped: 0
        } if result[date].nil?
        result[date][status] += 1
      end
      result.values.reverse!
    end


    def abnormal_builds
      standart_deviation = get_standart_deviation
      builds_per_day.map do |row|
        {
            failed: ((row[:failed] > standart_deviation) ? row[:failed] : 0),
            created_at: row[:created_at]
        }
      end
    end

    private
    def get_standart_deviation
      failing_builds = builds_per_day.map do |row|
        row[:failed]
      end
      n_minus_1 = (failing_builds.count-1)
      arithmetic_mean = failing_builds.inject(0) { |sum, x| sum + x }/failing_builds.count
      squared_dispersion = failing_builds.inject(0) { |sum, x| sum + (x- arithmetic_mean)**2 }

      Math.sqrt(squared_dispersion/n_minus_1)
    end

    def filter row
      time = row[FIELDS[:CREATED_AT]].to_time
      time > @start && time < @end.to_time
    end
  end
end