class DataController < ApplicationController
  require 'csv'
  require 'chart_data'
  SOURCE = 'public/store/session_history.csv'

  def show
    render json: get_data
  end


  def get_data
    csv_text = File.read(Rails.root.join(SOURCE))
    data = CSV.parse(csv_text)
    data.shift
    ChartData::Api.new(data, start_date, end_date).send type
  end

  private
  def type
    params.require :type
  end

  def start_date
    params.require :start
  end

  def end_date
    params.require :end
  end
end
