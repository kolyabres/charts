function charts(element, date_lement) {
    this.element = element;
    this.date_lement = date_lement;
    this.init_datepicker();
}
charts.prototype = {
    chart: {},
    init_datepicker: function () {
        var self = this;
        self.end_date = moment().toDate();
        self.start_date = moment().add(-4, 'years').toDate();
        self.daterangepicker = $("#" + self.date_lement).daterangepicker({
                startDate: self.start_date,
                endDate: self.end_date,
            }, function (start, end) {
                self.load_data(self.type_loaded, start.toDate(), end.toDate());
            }
        );
    },
    load_data: function (type, start, end) {
        var self = this;
        self.type_loaded = type;
        var start = start || self.start_date;
        var end = end || self.end_date;
        $.ajax({
            type: "GET",
            dataType: 'json',
            url: "/data",
            data: {type: type, start: start, end: end}
        }).done(function (data) {
            if (data.length) {
                self.chart.setData(data);
            } else {
                alert('data is empty, select another time interval');
            }
        }).fail(function () {
            alert('Failed to load data');
        });
    },
    chart_build_duration: function () {
        var self = this;
        self.destroy()
        self.chart = new Morris.Line({
            element: self.element,
            xkey: 'created_at',
            ykeys: ['duration'],
            labels: ['Build duration']
        });
        self.load_data('build_duration')
    },
    chart_builds_per_day: function () {
        var self = this;
        self.destroy()
        self.chart = new Morris.Bar({
            element: self.element,
            stacked: true,
            xkey: 'created_at',
            ykeys: ['failed', 'error', 'passed'],
            labels: ['Failed', 'Error', 'Passed'],
            //barColors: ['red', 'yellow', 'green']
        });
        self.load_data('builds_per_day')
    },
    chart_abnormal_builds: function () {
        var self = this;
        self.destroy()
        self.chart = new Morris.Line({
            element: self.element,
            xkey: 'created_at',
            ykeys: ['failed'],
            labels: ['Abnormal builds']
        });
        self.load_data('abnormal_builds')
    },
    destroy: function () {
        $('#' + this.element).empty();
    }

};

$(document).ready(function () {
    window.c = new charts('chart', 'daterange');
    window.c.chart_build_duration();
});